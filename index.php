<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a responsive product landing page.">
    <title>Cipher - Encryption/Decryption</title>

    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-" crossorigin="anonymous">

    <!--[if lte IE 8]>
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-old-ie-min.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
    <!--<![endif]-->

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
            <link rel="stylesheet" href="css/layouts/marketing.css">
        <!--<![endif]-->
</head>
<body>



<div class="splash-container">
    <div class="splash" style="height:80%;top:0">
        <h1 class="splash-head">cipher</h1>
        <p class="splash-subhead">
          <form class="pure-form" method="post" style="color:#fff;">
              <fieldset>
                  <?php
                    if(isset($_POST['data'])){ $data = $_POST['data']; }else{ $data = ''; }
                    if(isset($_POST['key'])){ $key = $_POST['key']; }else{ $key = ''; }
                    if(isset($_POST['iv'])){ $ivi = $_POST['iv']; }else{ $ivi = ''; }
                    $methods = openssl_get_cipher_methods();
                  ?>

                  <input type="text" placeholder="Data String" name="data" id="data" value="<?php echo $data; ?>" style="color:#000">
                  <input type="text" placeholder="Encryption Key" name="key" id="key" value="<?php echo $key; ?>" style="color:#000">
                  <input type="text" placeholder="Initialisation Vector" name="iv" id="iv" value="<?php echo $ivi; ?>" style="color:#000">

                  <?php

                      $methodFamilies = array();

                      foreach($methods as $method){
                        $methodFamily = explode("-",$method)[0];
                        if(!in_array($methodFamily,$methodFamilies)){
                          $methodFamilies[] = $methodFamily;
                        }
                      }

                      foreach($methodFamilies as $methodFamily){
                          if(isset($_POST[$methodFamily])){ $checked = "checked"; }else{ $checked = ""; }
                          echo "<label style=\"display:inline-block;color:#fff;margin:0.5em;border:1px solid #fff;border-radius:2px;padding:0.5em;\" for=\"".$methodFamily."\" class=\"pure-checkbox\"><input id=\"".$methodFamily."\" name=\"".$methodFamily."\" type=\"checkbox\" value=\"".$methodFamily."\" ".$checked." />".$methodFamily."</label>";
                      }
                  ?>
                  <button type="submit" class="pure-button" style="background:#33FF33;margin:1em">Run encryption/decryption</button>
                </fieldset>
            </form>
        </p>
    </div>
</div>

<div class="content-wrapper">
    <div class="content" style="text-align:center">
        <h2 class="content-head is-center">Data Hash</h2>
        <div class="pure-g">
          <div class="pure-u-1">
            <h3 class="content-subhead">SHA</h3>
            <table class="pure-table pure-table-bordered" style="margin:1em auto;">
              <tbody>
                  <tr>
                      <td>SHA1</td>
                      <td><?php echo hash("sha1",$data); ?></td>
                  </tr>
                  <tr>
                      <td>SHA256</td>
                      <td><?php echo hash("sha256",$data); ?></td>
                  </tr>
                  <tr>
                      <td>SHA384</td>
                      <td><?php echo hash("sha384",$data); ?></td>
                  </tr>
                  <tr>
                      <td>SHA512</td>
                      <td><?php echo hash("sha512",$data); ?></td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div class="pure-u-1 pure-u-md-1-3">
            <h3 class="content-subhead">MD5</h3>
            <p><?php echo hash("md5",$data); ?></p>
          </div>
          <div class="pure-u-1 pure-u-md-1-3">
            <h3 class="content-subhead">BASE64</h3>
            <table class="pure-table pure-table-bordered" style="margin:1em auto;width:90%">
              <tbody>
                <tr>
                    <td style="width:30%">Encoded</td>
                    <td><?php echo base64_encode($data); ?></td>
                </tr>
                <tr>
                    <td>Decoded</td>
                    <td><?php echo base64_decode($data); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="pure-u-1 pure-u-md-1-3">
            <h3 class="content-subhead">CRC32</h3>
            <p><?php echo hash("crc32",$data); ?></p>
          </div>
        </div>
        <h2 class="content-head is-center">Results</h2>

        <div class="pure-g">

        <?php
          if(strlen($data)>0){

            foreach($methodFamilies as $methodFamily){

              if(isset($_POST[$methodFamily])){
                  $methodStem = $_POST[$methodFamily];

                  foreach($methods as $method){
                    $methodFamilyStem = explode("-",$method)[0];
                    if($methodFamilyStem == $methodStem){
                        echo '<div class="pure-u-1 pure-u-md-1-3">';
                        echo '<h3 class="content-subhead">'.$method.'</h3>';
                        if($ivi){ $iv = $ivi; }else{ $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method)); }
                        $decryptresult = openssl_decrypt( $data , $method , $key, 0, $iv );
                        $encryptresult = openssl_encrypt( $data , $method , $key, 0, $iv );

                        echo '<table class="pure-table pure-table-bordered" style="margin:1em auto;width:90%">
                          <tbody>
                            <tr>
                                <td style="width:30%">Initialisation Vector</td>
                                <td>'.$iv.'</td>
                            </tr>
                            <tr>
                                <td>Encryption Result</td>
                                <td>'.$encryptresult.'</td>
                            </tr>
                            <tr>
                                <td>Decryption Result</td>
                                <td>'.$decryptresult.'</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>';
                    }
                  }
              }

            }

          }
        ?>

        </div>

    </div>

</div>




</body>
</html>
